import folium
import geopandas as gpd

# Charger les données des pays à partir du fichier JSON
data = gpd.read_file("C:/Users/Khalil/Desktop/pays.json")

# Sélectionner la France
france = data.loc[data['name_fr'] == 'France']

# Créer une carte
m = folium.Map(location=[48.8566, 2.3522], zoom_start=2)

# Ajouter la France à la carte en bleu
folium.GeoJson(france,
               style_function=lambda feature: {'fillColor': 'blue', 'color': 'blue'}).add_to(m)

# Enregistrer la carte
m.save("france.html")


