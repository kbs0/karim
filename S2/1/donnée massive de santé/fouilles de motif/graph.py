#graph orienté ou non ?
import networkx as nx
# pour creer un graph vierge
G=nx.Graph()
print(G.nodes(),G.edges())
#inserer des noeuds
G.add_nodes_from([12,13])
print(G.nodes)
print('number of nodes in the graph :',G.number_of_nodes())
#pour reinitialiser le graph
G.clear()
G.add_nodes_from([1,2,3])
#on ajoute ici un lien entre 1 et 2
G.add_edge(1,2)
print(G.nodes(),G.edges())
#
G.remove_edge(1,2)

G.add_edges_from([(1,2),(1,3)])
print(G.edges())
#ajouter un lien qui existe deja sur nx ca ne multiplie pas les lien contrairement a igraph
G=nx.path_graph(5)
G.add_node("spam")
print(G.nodes)
G.add_nodes_from("spam")
print(G.nodes) # !!!
print(G.number_of_edges())
print(G.edges())
print(G.degree())
#Les voisins
print(G.neighbors(1))#pb affichage
print(list(G.neighbors(1)))

import numpy as np
adj=np.array([[0,1,1],[1,0,1],[1,1,0]])
print(adj)
G=nx.from_numpy_matrix(adj)
print("coucou")
print(G.nodes)

#visualiser mtnt 
import matplotlib.pyplot as plt
nx.draw_networkx(G,with_labels=True)
plt.show()
#on passe sur R