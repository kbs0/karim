import folium

#Création de la carte
m = folium.Map(location=[48.8566, 2.3522], zoom_start=2)

#Données des pays
data = {'France': [48.8566, 2.3522],
        'Angleterre': [51.509865, -0.118092],
        'Corée du Sud': [37.566535, 126.977969],
        'Espagne': [40.4168, -3.7038],
        'Etats-Unis': [37.0902, -95.7129],
        'Chypre': [35.126413, 33.429859]}

#Ajout des marqueurs
for country, coord in data.items():
    folium.Marker(location=coord,
                  popup=country,
                  icon=folium.Icon(color=('green' if country == 'France' else
                                        'red' if country == 'Angleterre' else
                                        'purple' if country == 'Corée du Sud' else
                                        'blue' if country == 'Espagne' else
                                        'orange' if country == 'Etats-Unis' else
                                        'gray'),
                                   icon='info-sign')).add_to(m)

m.save('index.html')

